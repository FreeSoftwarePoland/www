---
title: "Książki i meteriały do czytania"
date: 2022-05-26T22:38:40+02:00
draft: false
---

## Książki

### Free Technology Academy:
* [Introduction to Software Development](/books/IntroductionToSoftwareDevelopment.pdf) [EN]
* [Introduction to Free Software](/books/IntroductionToFreeSoftware.pdf) [EN]
* [Hacker Ethic](/books/TheHackerEthicAndTheSpiritOfTheInformationAge.pdf) [EN]

### Inne
* [libreplanet](https://libreplanet.org/wiki/Table_of_contents) [EN]
* [The Hitchhiker’s Guide to Online Anonymity](https://anonymousplanet-ng.org/) [EN]

## Blogi:
* TODO

## Artykuły:
* [The Catedral and the Bazar. Eric Raymond](/books/TheCathedralAndTheBazar.pdf) [EN]
