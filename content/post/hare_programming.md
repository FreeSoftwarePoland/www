---
title: "Język programowania Hare"
date: 2022-12-11T16:41:26+01:00
tags: ["hare", "programowanie"]
draft: false
---

# Język C

Na początek nieco historii odnośnie programowania. Pierwsze języki pojawiły się bardzo dawno temu i
dziś niewiele wspomina się o niektórych z nich [Historia języków programowania](https://en.wikipedia.org/wiki/Timeline_of_programming_languages).
Jednak można między innymi zauważyć, że język C pomimo wieku nadal utrzymuje się przy życiu.

Sam język C jest rozwijany dziś i przechodzi powolną ewolucje, lecz może przeanalizujmy krótko jak to się odbywa.
Otóż język C na początku miał swojego twórce Denisa Ritchie, który go stworzył i wykorzystywał przy programowaniu systemów operacyjnych
i nie tylko.

Obecnie język posiada specyfikacje i jest współtworzony przez otwartą grupe roboczą. [Specyfikacja języka C](https://www.open-std.org/JTC1/SC22/WG14/www/docs/n2310.pdf)
Grupa robocza składa się z ludzi z różnych firm i organizacji, którzy spotykają się proponują zmiany i dyskutują o tym,
jak język będzie ewoluował. Tworzenie standardu języka to ważny proces, który wymaga ostrożnych decyzji i sporego doświadczenia.
Aby to co zostało wprowadzone, nie musiało być wycofywane albo nie wprowadziło zamieszania. Pytanie co było takie unikatowe w języku C, że
zostało podchwycone przez środowska wolnego oprogramowania?

Język C doczekał się ponownego wydania podręcznika swojego w sferze GNU [C Reference](https://git.savannah.nongnu.org/cgit/c-intro-and-ref.git/tree/c.texi).
W którym zapisane jest, że język jest portowalnym assemblerem. Co uważam, za ważne to jest otwarta specyfikacja. Dlaczego?
Ponieważ nie wszystkie są otwarte i bezpłatne. Brak dostępności specyfikacji tak naprawde, czyni język trochę bezużytecznym, bo istnieje tylko kompilator, który można przepisać.
Lub może ten kompilator wcale nie istnieć, posiadamy więc jedynie podręcznikowy opis języka.

Ale to nie koniec pułapek. Język może zastrzegać prawa do posiadania wielu kompilatorów i uznawać tylko jeden i jedną nazwe.
Ponadto może nie posiadać specyfikacji, albo jest ona odpłatna. Może również posiadać implementacje zamkniętą lub otwartą ale z patentami.
I to nie są mrzonki! Tak się faktycznie dzieje, istnieją języki jak Rust, C#, Java i inne. Posiadają one swoje ograniczenia, które pozostawiam do samodzielnego zbadania.


W dalszej części spróbujmy przejść do samej problematyki języka. Otóż z jakiegoś powodu powstają nowe języki, czasem są to bardzo zaawansowane rozwiązania,
a czasem są to języki typu lepsze C. I tutaj warto się zatrzymać. Skoro okazało się, że C ma swoje niedociągnięcia, dlaczego wciąż przy nim stoimy.
Obawiam się, że chodzi tutaj o alternatywę, która istnieje, jest popularna i dojrzała. Jednak, iż stoimy w czasach wielkiego biznesu.
Nie wszystko może się rozwijać bez nakładów finansowych. Dodatkowo niektóre nowości wcale nie są dobre. 
Język C w zasadzie obrał strategie powolnego rozwoju i obserwacji.


## Czy warto próbować czegoś innego?

Według mnie tak. Jeśli już potrafimy rozpoznać czy dany język jest rozwijany według zasad etycznych wolnego oprogramowania.
Warto się zainteresować czymś, co ma potencjał. Możliwe, że z punktu biznesowego nie ma to sensu. Ale mimo wszystko warto badać temat.
Chociaż dlatego, że nagle wolna technologia nie zniknie tylko pozostawi swój ślad w przestrzeni publicznej i ten ślad będzie zawierać pare naszych odcisków.

Jeśli udało mi się już przekonać co do sensowności to zajrzymy do języka Hare.


# Hare

Hare jest niskopoziomowym językiem programowania, bardzo minimalistycznym, który ma być lepszym C. Jednak twórcy wcale nie promują go w sposób chamski, aby przepisywać wszystko do Hare. Język rozwija się sam dla siebie. Posiada specyfikacje [Specyfikacja Hare](https://harelang.org/specification/). Posiada opis [Hare Wstęp do programowania](https://harelang.org/tutorials/introduction/).
Ma działający kompilator i kilka projektów rozwijanych. Sam kompilator jest oparty na backendzie QBE [QBE Backend do kompilatorów języków programowania](https://c9x.me/compile/). Rozwijany jest społecznie przy wsparciu jednego autora "Drew DeVault". Sam język jest mały i nudny [Hare jest nudny wpis z bloga](https://harelang.org/blog/2022-11-27-hare-is-boring/). Jednak tak jak C powinien stać się językiem, za pomocą, którego będzie można stworzyć stabilny i wydajny kod.

Na początek polecam zapoznać się czym jest Hare z krótkiej prezentacji [Hare Prezentacja Wstępna](https://spacepub.space/w/4b848922-e752-4065-baa3-f18afba13edb).

Z racji, że Hare może nie posiadać wsparcia do wszystkich dystrybucji stworzyłem krótki skrypt, który instaluje go lokalnie: [Skrypt do stawiania Hare lokalnie](https://codeberg.org/damaxi/harelocal).

Kilka słów podsumowujących o filozofii. Warto interesować się technologiami niszowymi z tego powodu, że inwestując nasz czas rozwiązujemy problemy, piszemy posty i podbijamy popularność rzeczy, które bez naszej cegiełki nigdy nie ujrzałyby światła dziennego. A gdy coś jest popularne tak jak C, to jest niezwykle ciężko to zastąpić.
I to jest właściwa siła przetargowa w świecie, gdy coś istnieje i może istnieć dzięki swojej popularności i dostępnych inżynierów oraz mnogości bibliotek na rynku.
Z tego powodu nowe języki inwestują wiele, by ludzie pisali w nich biblioteki. Jednak dla ludzi, którzy rozumieją filozofię wolnego oprogramowania. Cechą nadrzędną pozostaje
**wolność**.

@damaxi