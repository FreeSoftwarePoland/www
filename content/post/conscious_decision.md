---
title: "Świadome wybory"
date: 2023-09-21
tags: ["filozofia"]
draft: false
---

## Świadome wybory, czyli o czym trzeba myśleć na zakupach

W tym poście chciałbym porozmawiać o wyborach, które podejmujemy. A dokładnie mowa o naszych zakupowych wyborach. W pewnym sensie wciąż jesteśmy ludźmi, którzy przyzwyczaili się do kupowania urządzeń, które z założenia posłużą nam krótko lub długo. Dawniej ludzie używali prymitywnych narzędzi, które były łatwe w naprawie i obsłudze. Później wstąpiliśmy do ery tanich surowców energetycznych, gdzie naprodukowaliśmy kupę śmieci, a rozwój postępował na tyle szybko, że "opłacało się" kupić nowszy model.

W pewnym sensie dziś dochodzimy do momentu, gdzie mniej lub bardziej. Produkty wyprodukowane 5 lat temu, mogą całkowicie zaspokoić i dzisiejszego użytkownika.

I tutaj pojawia się problem ekonomiczny, jeśli określimy, że dla ilości ludzi X i to jeszcze przy malejącej populacji zachodzi potrzeba posiadania rzeczy Y i Z. To przy malejącej populacji potrzeba produkcji mocno zanika.

Oczywiście gdy analizujemy przykład telefonu to, co może się zużyć to:

- bateria
- karta pamięci


A w przypadku słuchawek to by była tylko bateria. A w przypadku monitora? No w zasadzie nic. Podobnie telewizor z reguły powinien nam długo służyć.

Stąd też jakiś czas temu pojawił się problem, że firmy zaczęły kombinować przy tworzeniu sprzętu i go postarzają celowo. Sprzęt może się zepsuć nawet gdy jest niedbale zrobiony, a części zamiennych brak, bądź ciężko jest dostać się do środka w warunkach domowych.

Drugi problem bardziej nowoczesny, jest fakt, że oprogramowanie, które dostajemy wraz ze sprzętem, jest do tego sprzętu przypisany i nie jesteśmy w stanie łatwo go zmienić. Co w przypadku telefonów stanowi to pewien problem.


## Wersje

Otóż producenci tworzą wersje systemów i oprogramowania i wsparcie. Gdy wsparcie się kończy, nie jesteśmy w stanie zaktualizować oprogramowania, a nowe aplikacje przestają działać. Mimo w pełni funkcjonalnego sprzętu pozostaje nam wymiana.


## Pułapki

Pułapka taniego sprzętu:
Gdy kupujemy sprzęt, który kusi nas ceną i posiada wszystko, co potrzebujemy, najczęściej z Chin. Możemy wpaść w tak zwaną zależność poprzez kupno pewnych usług przypisanych do konta. Otóż kupujemy oprogramowanie jako serwis, należy ono do nas, ale uwaga działa wyłącznie na danym sprzęcie i jest przypisane do profilu. Na dodatek oprogramowanie jest drogie, bo można je instalować wyłącznie przez autoryzowany serwis. A wymiana na sprzęt innej firmy wzbudza w nas żal i politowanie, bo posiadamy konto z oprogramowaniem droższym niż sprzęt.

Kolejny problem to pułapka chińskiego taniego sprzętu. Producent w tym wypadku myśli w sposób. I tak mnie kupisz. Po jakimś czasie oprogramowanie zainstalowane przez producenta zamienia się w złośliwe oprogramowanie, czyli np. aplikacje systemowe zaczynają wyświetlać nam reklamy. Może nastąpić celowe spowolnienie sprzętu i reklamy jak go przyśpieszyć przy pomocy aplikacji. Handel danymi. Oraz ze względu na kraj produkcji brak całkowitej ochrony prywatności.


## Epilog

Na koniec moja osobista przygoda.
Bardzo długo korzystałem z telefonu Xiaomi, który pozostał mi wierny jako urządzenie i trwały. Jednak po pewnym czasie zauważyłem, że aktualizacje zmieniają aplikacje i zalewają mnie reklamy. Po pewnym czasie miałem też problem z zainstalowaniem nowych aplikacji, ponieważ Android był zbyt stary. Na koniec próby flashowania zakończyły się fiaskiem, ponieważ producent nieźle się zabezpieczył i po wielu próbach poddałem się. Oczywiście zupełnie tego się nie spodziewałem i nie jestem typem co odkłada co chwilę na nowy telefon, bo zakładam, że o ile rzecz nie zepsułem mechanicznie to powinna działać długo. A jednak nie do końca tak jest.

Podsumowując, ze względu na obfitość dóbr wchodzimy, w coraz bardziej agresywną politykę opanowywania rynku poprzez różne sztuczki, za które sami płacimy, bo czy ktoś zadał pytanie ile firm musi kosztować tworzenie oprogramowania zabezpieczającego sprzęt?
A czasem jest to mowa nie tylko oprogramowania, ale i specjalnego sprzętu szyfrującego.
Urządzenia są inteligentne, posiadają mnóstwo funkcji, które firma chce byśmy potrzebowali i są drogie. Z drugiej strony alternatywy są często ignorowane, gdy porównywarki internetowe wylistowywują cenę do tego co posiada. Praktycznie całkowicie ignorując, żywotność, naprawy i otwartość rozwiązania.

__@andrzejku__
