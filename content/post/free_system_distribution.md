---
title: "O wolnym systemie operacyjnym"
date: 2022-07-19T13:08:38+02:00
tags: [system]
draft: false
---

## Co to jest wolny system operacyjny?

Zwykle przyjęliśmy upraszczać rzeczy i pomijać detale. Często zatem słyszymy Linux i od razu kojarzymy,
że to coś wolnego. Niestety Linux sam w sobie nie jest niczym niezwykłym jeszcze. Faktycznie jesteśmy w
stanie podejrzeć jego [źródła](https://github.com/torvalds/linux).
Jednak równie dobrze, można podejrzeć źródła projektów otwartych Microsoftu i Apple.

### Diabeł tkwi w szczegółach.

Otwartość stała się wartością atrakcyjną dla branży technologicznej.
Ponieważ inżynierowie mogą testować, sprawdzać, naprawiać oraz ulepszać oprogramowanie za darmo.
Albo nie całkowicie za darmo, bo mogą starać się o pracę w danej branży, bądź technologii dzięki wiedzy, którą zdobyli.
Pewne funkcjonalności natomiast, które są szczególnie wrażliwe dla firmy pozostają zamknięte.
Uwolnienie ich, może przykładowo doprowadzić do zerwania monopolu.


Przeanalizujmy pewien przykład, kupujemy urządzenie Apple, na którym świetnie działa OS X.
Natomiast na ten moment firma Apple dogadała się z firmą Microsoft, aby udostępnić jej możliwość używania Windowsa na
swoim produkcie. Jednak po paru latach użytkowania dowiadujemy się, że istnieje system Linux,
a konkretnie dystrybucja, która posiada znakomite aplikacje i coś, co stało się dla nas atrakcyjne.
Niestety próba instalacji zawodzi, ponieważ pomimo standardowych komponentów procesora Intel i karty Nvidia.
Pewne podzespoły nie są rozpoznawane przez jądro Linuxa. A producent nie postarał się ani aby
udostępnić informacje jak działa np. karta sieciowa, ani nie ma zamkniętych sterowników.


Ba, nawet gdyby udostępnił zamknięty sterownik. Może się okazać, że nie działa on tak dobrze,
jak w innym systemie, ponieważ postanowił on w pewnym momencie ze względu na koszta porzucić wsparcie.
W takim wypadku niewiele jesteśmy w stanie zrobić. (istnieją pewne nieproste techniki, których nie będziemy omawiać)

### Nasuwa się pytanie, czym byłby w pełni otwarty system?

A no taki, gdzie każdy zestaw komend wysłany do procesora jest przedstawiony w postaci czytelnego kodu źródłowego.
I może być przeanalizowany oraz poprawiony przez człowieka. Gdy warunki sprzętowe zostają spełnione jesteśmy w
stanie nie tylko używać Linuxa ale również innych systemów jak [GNU Hurd](https://www.gnu.org/software/hurd/) lub inny z własną architekturą.

### Czy Linux jest w pełni wolny?

Otóż nie. Jądro Linuxa daje przyzwolenie na użytkowanie go wraz z zamkniętymi [blobami](https://puri.sm/learn/blobs/).
Są to zamknięte miejsca systemu, w których nie jesteśmy w stanie się dowiedzieć co tak naprawdę one robią.
A zważając, że jest to warstwa systemu operacyjnego. Mogą one naprawdę wiele.
Naprzykład niesławne [backdoory](https://www.techworm.net/2015/08/lenovo-pcs-and-laptops-seem-to-have-a-bios-level-backdoor.html),
czyli tylne furtki, przez które ktoś obcy może "wejść" do naszego laptopa, a których nie da się pozbyć bez wyłączenia np. karty sieciowej.
Czy [spyware](https://www.gnu.org/philosophy/ubuntu-spyware.en.html), czyli szpiegowanie i przekazywanie o nas informacji najczęściej w celach marketingowych.

Raz na jakiś czas są przeprowadzane audyty bezpieczeństwa, które ujawniają przypadkiem tego typu afery.

### Pewna część świata się "zbroi" i potrzebuje wiedzieć o nas coraz więcej.

Aby pilnować bezpieczeństwa na komputerach domowych. Na szczęście nie jesteśmy zupełnie osamotnieni.
Jednak, aby to zrealizować musimy "dbać" o naszą wolność. W przypadku Linuxa jesteśmy w stanie użyć
wersji tzw. [linux-libre](https://www.fsfla.org/ikiwiki/selibre/linux-libre/).
Nie twierdzę, że należy od razu sprzedać dotychczasowy komputer i zakupić taki,
który jest wolny (co jest możliwe ale nie proste). Ale dzięki temu zabiegowi jesteśmy w stanie się dowiedzieć, możliwe po raz pierwszy.
Które części naszego sprzętu są zamknięte. I skorzystać świadomie z rozwiązań non-free dla Linuxa.

### Przechodząc ze strefy systemu do warstwy aplikacyjnej.

W tej części tak naprawdę kontrola jest o wiele łatwiejsza. Dlaczego?
Ponieważ systemy uniksopodobne implementują abstrakcje zwaną [POSIX](https://en.wikipedia.org/wiki/POSIX).
Która to jest używana w sposób transparentny przez języki programowania oraz aplikacje.
W tej strefie nie ma zależności sprzętowej. Więc wybór alternatyw, może być dokonany wprost.

### Jednak i tutaj można nadepnąć na pułapkę.

Sporo dystrybucji instaluje wprost bądź nie zwraca uwagi na typ oprogramowania, lub w najlepszych w przypadku
jak w Debian przywiązuje uwagę na rozróżnienie [oprogramowania](https://www.debian.org/intro/free).

Zważając na powyższe dobrze jest się zapoznać z [Free System Distribution Guideline](https://www.gnu.org/distros/free-system-distribution-guidelines.en.html).
Jest w pewnym sensie dokument opisujący czym powinien być wolny system z punktu widzenia praktyk obecnie stosowanych, częściowo opisanych powyżej.

Obecnie istnieje kilka systemów, które pilnują naszej wolności w szerokim zakresie [liste](https://www.gnu.org/distros/free-distros.html).
Część z nich jak te oparte na Debianie są user-friendly, a także w pełni funkcjonalne i niezawodne.

### Dlaczego warto zwracać szczególną ostrożność?

Myślę, że chodzi o świadomość. Budowanie świadomości na temat, z czego korzystamy i z czego możemy korzystać.
Inna sprawa, czego brakuje w wolnym świecie i kto wie, może zmotywuje nas to do zaangażowania się w stworzenie lub sfinansowanie brakującego rozwiązania.

Także, przez zbudowanie świadomości, możemy znaleźć się w sytuacji, w której będziemy w stanie uczulić innych użytkowników.

---
Dany tekst jest ogólnym przedstawieniem czym jest wolny system operacyjny.

@andrzejku
