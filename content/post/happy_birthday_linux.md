---
title: "Wszystkiego Najlepszego z Okazji 31 urodzin Linux'a"
date: 2022-08-25
tags: ["linux"]
draft: false
---

Historia przypadkowego odkrycia zaczyna się dość skromnie, bo pewnego dnia pewien student postanowił stworzyć swój system operacyjny.

```
    Hello everybody out there using minix-

    I’m doing a (free) operating system (just a hobby, won’t be big and professional like gnu) for 386(486) AT clones. This has been brewing since april, and is starting to get ready. I’d like any feedback on things people like/dislike in minix; as my OS resembles it somewhat (same physical layout of the file-sytem due to practical reasons)among other things.
    Linus Torvalds (1991)
```

Interesujący jest fakt, że Linus wiedział i słyszał o wolnym oprogramowaniu jednak nie był, aż takim entuzjastą. Chociaż jak sam przyznaje:
```
"I really think the license has been one of the defining factors in the success of Linux because it enforced that you have to give back, which meant that the fragmentation has never been something that has been viable from a technical standpoint."
```

W pewien sposób jest to właśnie odzwierciedlenie przypadkowego sukcesu, sposobu podejścia, gdzie użytkownik jest zmuszony do "dania" coś w zamian. Za udostępnienie mu soft'u, nad którym inni spędzili część swojego życia. Chociaż właściwie do dziś Linux staje w opozycji do wielu projektów podobnych mu jak: OSX, Windows, Fuchsia itp. Ze strony hi-techu płynęły głosy, że Linux to komunizm, bo w świecie nic może być darmowe.

```
Mr. Gates’ secret is out now–he too was a “communist;” he, too, recognized that software patents were harmful–until Microsoft became one of these giants."
```

Jednak historia ta pokazuje, że można stworzyć coś i oprzeć się biznesowi. Ba, można stworzyć inny rodzaj biznesu.

W którym użytkownik wie co używa, a zatem wie za co płaci. Czuje wartość tego, może posiąść wiedzę i usprawnić istniejący produkt. No i najważniejsze, że wiedza, nad którą pracuje pozostanie.

Niestety, ale w dzisiejszym systemie gospodarczym, świat dąży do maksymalizacji zysków. Przy tym stopień skomplikowania narzędzi, z których korzystamy i wiedzy jest bardzo wysoki. Uczymy się całe życie w domu w szkole na uczelniach wyższych, aby na końcu te wiele lat nauki sprzedać firmie za miesięczne wynagrodzenie. Ciekawe, że wśród piłkarzy, gdy jakiś klub kupuje gracza. Wszystkie poprzednie kluby dostają swój mały udział z transakcji.

Wiedza i doświadczenie nabyte staje się własnością firmy. Firma następnie pragnie mieć wyłączność do tej wiedzy i sprzedawać swój produkt najdrożej jak się da.

Z takimi przypadkami wiąże się sporo problemów. Pierwszy problem to wyłączność wiedzy.

Dzięki rozpowszechnieniu informacji jesteśmy w stanie w łatwy sposób wyrównać nierówności wiedzowe na świecie i następnie czerpać benefity z tego, że znajdują się osoby, które chcą udoskonalić boć spopularyzować daną wiedzę. W ten sposób Linus jest dobrym wujem, który też przegląda wiele zmian nadesłanych przez użytkowników i firmy.

Kolejną sprawą jest brak wyłączności na sprzedaż danego rozwiązania. Dzięki alternatywie Linux zadomowił się na wielu platformach są to drobne urządzenia elektroniczne, smartfony, auta, samoloty itp. Dostaliśmy dostęp do tanich serwerów i rozwiązań chmurowych. Co nie jest aż takie oczywiste, jeśli mówimy o branżach bardziej skomplikowanych niż tworzenie softu. Np. w branży medycznej co chwilę zbiera się w Polsce na leki takie jak Zolgensma wartych 10 mln., które wręcz ratują życie, lecz nie są produkowane w Polsce. Same patenty to są często pół wieku życia takiego właśnie Linux'a.

Są to dość głębokie rozważania, ale należy podkreślić fakt, że przy wysiłku jednej osoby i wsparciu wielu. Może powstać coś, co w oczach innych nigdy nie miałoby szansy na powodzenie.

Wszystkiego Najlepszego raz jeszcze!

@damaxi
